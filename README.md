# Semaine Special 1 - LEGO LOGO

Projet de 1ère année de DUT, 1ère semaine spécial.

le but de ce projet est de faire un logiciel en C++ avec lequel on peut, via une interface, créer un parcours et y installer des obstacles pour ensuite le récupérer et l’envoyer dans un robot.

Programme C++ sans programmation object.

𝘌𝘯 𝘤𝘰𝘭𝘭𝘢𝘣𝘰𝘳𝘢𝘵𝘪𝘰𝘯 𝘢𝘷𝘦𝘤 CHARNAY Louis, MAADOUR Dalil

## Tester le projet

1. Cloner le projet 
2. Lancer le projet avec Visual Studio 2019 ou plus
3. Faite **run** ou **debug**
4. Si des erreurs apparaissent, essayer d'installer les packages NuGet :
    - sdl2
    - sdl2.nuget
    - sdl2_image.nuget
    - sdl2_ttf.nuget

## Données du projet GitLab original

Projet original disponible sur [IUT-bg GitLab](https://iutbg-gitlab.iutbourg.univ-lyon1.fr/).

**Vous devez posséder le vpn de l'IUT-Lyon1 et un compte de l'université pour accéder au projet original**

Le projet original est privé.

| Commits | 91 |
|---|---|
| Branches | 4 |
